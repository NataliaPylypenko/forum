<?php

function getAllThemes($db, $lim, $offset) // правильно
{
    $sql = "SELECT * FROM themes ORDER BY `date` DESC LIMIT $lim OFFSET $offset";
    $sth = $db->prepare($sql);
    $sth->execute();
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


function getByIdThemes($id, $db) // getThemeById($db, $id)
{
    $sql = "SELECT * FROM themes WHERE id = ?";
    $sth = $db->prepare($sql);
    $sth->execute([$id]);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    return $data;
}


function getCommentsByThemes($db, $id, $lim, $offset) //getCommentsByTopicId($db, $topic_id, $lim, $offset)
{
    $sql = "SELECT * FROM comments WHERE topic_id = ? LIMIT $lim OFFSET $offset";
    $sth = $db->prepare($sql);
    $sth->execute([$id]);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


function getThemeByTitle($db, $topic_title) //getThemeByTopicTitle
{
    $sql = "SELECT topic_title FROM themes WHERE topic_title = ?";
    $sth = $db->prepare($sql);
    $sth->execute([$topic_title]);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


function saveByThemes($autor_name, $topic_title, $description, $db) // saveTheme($db, ...)
{
    $stmt = $db->prepare("INSERT INTO themes (autor_name, topic_title, description) VALUES (:autor_name, :topic_title, :description)");
    $stmt->bindParam(':autor_name', $autor_name, PDO::FETCH_ASSOC);
    $stmt->bindParam(':topic_title', $topic_title, PDO::FETCH_ASSOC);
    $stmt->bindParam(':description', $description, PDO::FETCH_ASSOC);
    $stmt->execute();
    return true;
}

function saveByComments($name, $answer, $id, $db) // saveComment($db, ...)
{
    $stmt = $db->prepare("INSERT INTO comments (name, answer, topic_id) VALUES (:name, :answer, :topic_id)");
    $stmt->bindParam(':name', $name, PDO::FETCH_ASSOC);
    $stmt->bindParam(':answer', $answer, PDO::FETCH_ASSOC);
    $stmt->bindParam(':topic_id', $id, PDO::FETCH_ASSOC);
    $stmt->execute();
    return true;
}


function countComment($db, $id) //getCountCommentByTopicId($db, $topic_id)
{
    $sql = "SELECT COUNT(*) as count FROM comments WHERE topic_id = ?";
    $sth = $db->prepare($sql);
    $sth->execute([$id]);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    return $data['count'];
}

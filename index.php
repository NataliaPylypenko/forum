<?php
require_once 'app.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="col-sm-6 col-sm-offset-3">
        <h1>Наш форум</h1>
        <p>Наш супер крутой форум посвящен анонимным рассказам и секретов реальных людей. Ничего более, только текст. Здесь вы можете поделиться своим откровением или просто излить душу незнакомым людям. Так же можете остаться просто читателем, поняв, что не все так уж и плохо в вашей жизни..</p>
        <h2>Темы форума</h2>

            <div class="comment-wrapper">
                <?php foreach ($descriptions as $description): ?>
                    <p><a name="view_note" target="_blank" href="http://localhost/forum/pageview?topic_id=<?= $description['id'] ?>"><?= $description['topic_title'] ?></a></p>
                    <p><b>Создана: </b><?= $description['date'] ?><b> Автор: </b><?= $description['autor_name'] ?></p>
                    <p><b>Количество ответов:</b><?=countComment($db, $description['id'])?></p>
                <?php endforeach ?>
            </div>


        <ul class="pagination">
            <?php if($page > 1): ?>
                <li><a href="?page=<?=$page-1?>"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>
            <?php endif;?>
            <?php for ($i = 1; $i <= $page_count; $i++){ ?>
                <li <?php if($page == $i) echo "class='active'";?> ><a href="?page=<?=$i?>"><?=$i?></a></li>
            <?php } ?>
            <?php if($page < $page_count): ?>
                <li><a href="?page=<?=$page+1?>"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
            <?php endif;?>
        </ul>

        <h2>Создать тему</h2>

        <?php if($show_message):?>
            <div class="alert alert-info">
                Тема успешно создана!
            </div>
        <?php endif;?>


        <?php if($theme_message):?>
            <div class="alert alert-danger">
                Тема с таким названием уже существует!
            </div>
        <?php endif;?>


        <form action="" method="POST">
            <div class="form-group">
                <label for="autor_name">Your name:</label>
                <input type="text" class="form-control" name="autor_name" id="autor_name">
            </div>
            <div class="form-group">
                <label for="topic_title">Topic title:</label>
                <input type="text" class="form-control" name="topic_title" id="topic_title">
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" rows="5" name="description" id="description"></textarea>
            </div>
            <button type="submit" name="save_description" class="btn btn-primary col-sm-12">Сохранить</button>
        </form>
    </div>
</div>

</body>
</html>

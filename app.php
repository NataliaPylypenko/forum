<?php
require_once 'forum.php';

$db = new PDO('mysql:host=localhost;dbname=forum', 'root', '');
$db->exec("set names utf8");

$theme_message = false;
$show_message = false;

if (isset($_POST['save_description'])) {
    $autor_name = ($_POST["autor_name"]); //author_name
    $topic_title = ($_POST["topic_title"]);
    $description = ($_POST["description"]);

    if(getThemeByTitle($db, $topic_title)){
        $theme_message = true;
    }

    if (isValid($autor_name) && isValid($topic_title) && isValid($description) && !$theme_message) {
        saveByThemes($autor_name, $topic_title, $description, $db);
        $show_message = true;
    }
}


function isValid($text)
{
    $text = trim($text);
    if (empty($text)) return false;
    return true;
}


$stmt = $db->query('SELECT * FROM themes');
$row_count = $stmt->rowCount();

$lim = 3;
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$offset = (($lim * $page) - $lim);
$page_count = ceil($row_count / $lim);

$id = []; //?  треба позначати що за айді, і для чого воно
if (isset($_GET['topic_id'])) {
    $by_id = getByIdThemes($_GET['topic_id'], $db);
    $id = $by_id['id'];

    $row_count = (int)countComment($db, $_GET['topic_id']);
    $lim = 3;
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    $offset = (($lim * $page) - $lim);
    $page_count = ceil($row_count / $lim);

    $getCommentsByThemes = getCommentsByThemes($db, $id, $lim, $offset);
}

$descriptions = getAllThemes($db, $lim, $offset);


if (isset($_POST['save_comment']) && !empty ($_POST["name"]) && !empty ($_POST["answer"])) {
    $name = ($_POST["name"]);
    $answer = ($_POST["answer"]);

    if (isValid($name) && isValid($answer)) {
        saveByComments($name, $answer, $id, $db);
        $show_message = true;
    }
}
?>
<?php
require_once 'app.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="col-sm-6 col-sm-offset-3">
        
        <div class="comment-wrapper">
            <h1>Тема №<?= $by_id['id'] ?></h1>
            <p><b>Создана: </b><?= $by_id['date'] ?> <b> Автор: </b><?= $by_id['autor_name'] ?></p>
            <p><b>Количество ответов: </b> <?=countComment($db, $by_id['id'])?>.<a name="view_note" target="_blank" href="http://localhost/forum/index"> Перейти на список тем. </a></p>        
        </div>

        <h3><?= $by_id['topic_title'] ?></h3>
        <p><?= $by_id['description'] ?></p>

            <h2>Ответы</h2>
            <div class="comment-wrapper">
                <?php foreach ($getCommentsByThemes as $getCommentsByTheme): ?>
                    <p><b><?= $getCommentsByTheme['date'] ?></b><?= $getCommentsByTheme['name'] ?></p>
                    <p><?= $getCommentsByTheme['answer'] ?></p>
                <?php endforeach ?>
            </div>





        <ul class="pagination">
            <?php if($page > 1): ?>
                <li><a href="http://localhost/forum/pageview?topic_id=<?= $by_id['id']?>&page=<?=$page-1?>"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>
            <?php endif;?>
            <?php for ($i = 1; $i <= $page_count; $i++){ ?>
                <li <?php if($page == $i) echo "class='active'";?> ><a href="http://localhost/forum/pageview?topic_id=<?= $by_id['id']?>&page=<?=$i?>"><?=$i?></a></li>
            <?php } ?>
            <?php if($page < $page_count): ?>
                <li><a href="http://localhost/forum/pageview?topic_id=<?= $by_id['id']?>&page=<?=$page+1?>"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
            <?php endif;?>
        </ul>

        <?php if($show_message):?>
            <div class="alert alert-info">
                Запись успешно сохранена!
            </div>
        <?php endif;?>


        <form action="" method="POST">
            <div class="form-group">
                <label for="usr">Your name:</label>
                <input type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="message">Your message:</label>
                <textarea class="form-control" rows="5" name="answer" id="answer"></textarea>
            </div>
            <button type="submit" name="save_comment" class="btn btn-primary col-sm-12">Сохранить</button>
        </form>
    </div>
</div>

</body>
</html>
